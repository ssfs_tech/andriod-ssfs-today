package org.ssfs.today_ssfs

import java.util.Calendar

/**
 * Created by brian on 9/28/17.
 */

class DateInfo {

    val dayOfWeek: Int
    val currentDay: Int
    private val dayOfMonth: Int
    private val month: Int
    private val year: Int
    private val calendar: Calendar

    val todaysDate: String
        get() = Integer.toString(month + 1) + "/" + Integer.toString(dayOfMonth) + "/" + Integer.toString(year)

    init {
        calendar = Calendar.getInstance()
        dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
        month = calendar.get(Calendar.MONTH)
        year = calendar.get(Calendar.YEAR)
        currentDay = dayOfWeek - 1
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)

    }

    fun getDateString(day: Int): String {
        return Integer.toString(month + 1) + "/" + Integer.toString(day) + "/" + Integer.toString(year)
    }

    val currentDate: Int
        get() = dayOfMonth


}
