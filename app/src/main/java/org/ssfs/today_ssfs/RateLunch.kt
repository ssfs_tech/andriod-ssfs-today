package org.ssfs.today_ssfs

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.RatingBar

class RateLunch : AppCompatActivity() {

    private var lunchRating: RatingBar? = null
    private var vegieRating: RatingBar? = null
    private var comment: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rate_lunch)
        lunchRating = findViewById(R.id.lunch_rating) as RatingBar
        vegieRating = findViewById(R.id.vegie_rating) as RatingBar
        comment = findViewById(R.id.comment_field) as EditText
    }

    fun submitRating(view: View) {
        var finalLunchRating = lunchRating!!.rating
        var finalVegieRating = vegieRating!!.rating
        var commentToBeSubmitted = comment!!.text

    }
}
