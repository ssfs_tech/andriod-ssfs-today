package org.ssfs.today_ssfs

/**
 * Created by wilkibr on 7/7/2016.
 */
interface AsyncResponse {

    fun processFinish(output: String)
}
